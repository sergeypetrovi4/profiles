//
//  Profile.swift
//  App
//
//  Created by Sergey Krasiuk on 23/04/2019.
//

import FluentSQLite
import Vapor

struct Profile {
    
    enum Social: Int, Content {
        case facebook = 0
        case dribble
        case behance
        case pinterest
        case instagram
        case vkontakte
        case deviantart
    }
    
    var name: String
    var specialization: String
    var descritpion: String
    
    var socials: [Social]
    
    var posts: Int
    var following: Int
    var followers: Int
    
    init(with name: String, specialization: String, description: String, socials: [Social], posts: Int, following: Int, followers: Int) {
        
        self.name = name
        self.specialization = specialization
        self.descritpion = description
        
        self.socials = socials
        
        self.posts = posts
        self.following = following
        self.followers = followers
    }
}

extension Profile: Content {}
